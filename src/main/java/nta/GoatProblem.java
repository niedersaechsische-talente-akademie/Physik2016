package nta;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Marco
 */
public class GoatProblem {

    private final Random random = new Random();

    private final int TEST_SIZE = (int) 1e4;

    public GoatProblem() {
        Map<Integer, Double> probabilities = new HashMap<>();
        int totalWins = 0;

        for (int i = 1; i <= TEST_SIZE; i++) {
            if (simulate(true)) totalWins++;
            probabilities.put(i, (double) totalWins / (double) i);
        }

        System.out.println(Util.mapToCsv(probabilities));
    }

    private boolean simulate(boolean swap) {
        boolean[] doors = new boolean[] { false, false, false };
        doors[random.nextInt(3)] = true;

        int chosen = random.nextInt(3);

        int open = 0;
        while (doors[open] || open == chosen) open++;

        int swapAlt = 3 - chosen - open;

        return swap ? doors[swapAlt] : doors[chosen];
    }

    public static void main(String[] args) {
        new GoatProblem();
    }
}
