package nta;

import java.util.Map;

/**
 * @author Marco
 */
public class Util {

    public static String mapToCsv(Map<?, ?> map) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            builder.append(entry.getKey()).append(",").append(entry.getValue()).append("\n");
        }

        return builder.toString();
    }
}
