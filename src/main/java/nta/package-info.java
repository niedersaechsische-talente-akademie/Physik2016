/**
 * Quick-n-dirty simulations for the Physics (Maths) workshop of the NTA 2016.
 * Putting lombok (see projectlombok.org) aside, the classes only use the Java API,
 * mostly the packages java.util and javafx.beans.
 *
 * The sources can be (re-)used however one desires as long as attribution to Marco David is given.
 */
package nta;
