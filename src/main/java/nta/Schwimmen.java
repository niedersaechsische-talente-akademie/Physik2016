package nta;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.isEqual;
import static nta.Schwimmen.Suit.*;
import static nta.Schwimmen.Value.*;

/**
 * @author Marco
 */
public class Schwimmen {

    public static final int TEST_LIMIT = (int) 1e6;

    public Schwimmen() {
        testCountPoints();

        List<Card> skat = new ArrayList<>(skatDeck());

        double total = 0;
        for (int t = 0; t < TEST_LIMIT; t++) {
            Collections.shuffle(skat);
            total += countPoints(skat.subList(0, 2));
        }

        System.out.println(total / TEST_LIMIT);
    }

    private Double countPoints(List<Card> cards) {
        // 33 and 30 1/2 points
        if (cards.stream().map(Card::getValue).filter(isEqual(cards.get(0).getValue()))
            .collect(Collectors.toList()).size() == cards.size()) {
            if (cards.get(0).getValue() == Value.ACE) return 33d;
            return 30.5d;
        }

        // calc per suit and then use max
        List<Double> points = new ArrayList<>();
        for (Suit suit : Suit.values()) {
            points.add(cards.stream().filter(card -> card.getSuit() == suit).map(Card::getValue).collect(Collectors.summingDouble(Value::getNumberValue)));
        }

        return Collections.max(points);
    }

    private void testCountPoints() {
        assert countPoints(Arrays.asList(new Card(CLUBS, JACK), new Card(CLUBS, KING), new Card(CLUBS, ACE))) == 31;
        assert countPoints(Arrays.asList(new Card(HEARTS, SEVEN), new Card(HEARTS, TEN), new Card(SPADES, ACE))) == 17;
        assert countPoints(Arrays.asList(new Card(DIAMONDS, NINE), new Card(CLUBS, NINE), new Card(SPADES, NINE))) == 30.5;
        assert countPoints(Arrays.asList(new Card(DIAMONDS, ACE), new Card(CLUBS, ACE), new Card(SPADES, ACE))) == 33;
    }

    enum Suit {
        CLUBS, SPADES, HEARTS, DIAMONDS
    }

    @Getter
    enum Value {
        SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(10), QUEEN(10), KING(10), ACE(11);

        private final int numberValue;

        Value(int numberValue) {
            this.numberValue = numberValue;
        }
    }

    @lombok.Value @RequiredArgsConstructor
    static class Card {
        private final Suit suit;
        private final Value value;
    }

    static List<Card> skatDeck() {
        List<Card> cards = new ArrayList<>();
        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                cards.add(new Card(suit, value));
            }
        }

        return cards;
    }

    public static void main(String[] args) {
        new Schwimmen();
    }
}
