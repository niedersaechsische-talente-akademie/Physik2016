package nta;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.*;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marco
 */
public class EuroChampionship {

    public static final double TEST_SIZE = 1e6;

    public EuroChampionship() {
        Map<String, Integer> wins = new HashMap<>();

        for (int i = 0; i < TEST_SIZE; i++) {
            setup();
            simulate();

            String winner = FINAL.winningTeamProperty().get().getName();
            if (wins.containsKey(winner)) {
                wins.put(winner, wins.get(winner) + 1);
            } else {
                wins.put(winner, 1);
            }
        }

        System.out.println(Util.mapToCsv(wins));
    }

    private void simulate() {
        for (Game game : E) {
            game.play();
        }

        for (Game game : Q) {
            game.play();
        }

        for (Game game : S) {
            game.play();
        }

        FINAL.play();
    }

    private void setup() {
        Team
            FRANCE = new Team("Frankreich", 4, 1),
            SWITZERLAND = new Team("Schweiz", 2, 1),
            WALES = new Team("Wales", 6, 3),
            ENGLAND = new Team("England", 3, 2),
            SLOWAKIA = new Team("Slowakei", 3, 3),
            GERMANY = new Team("Deutschland", 3, 0),
            POLAND = new Team("Polen", 2, 0),
            NORTH_IRELAND = new Team("Nordirland", 2, 2),
            KROATIA = new Team("Kroatien", 5, 3),
            SPAIN = new Team("Spanien", 5, 2),
            ITALY = new Team("Italien", 3, 1),
            BELGIUM = new Team("Belgien", 4, 2),
            IRELAND = new Team("Irland", 2, 4),
            HUNGARY = new Team("Ungarn", 6, 4),
            ICELAND = new Team("Island", 4, 3),
            PORTUGAL = new Team("Portugal", 4, 4);

        E = new Game[] {
            new Game(SWITZERLAND, POLAND),
            new Game(KROATIA, PORTUGAL),
            new Game(WALES, NORTH_IRELAND),
            new Game(HUNGARY, BELGIUM),
            new Game(GERMANY, SLOWAKIA),
            new Game(ITALY, SPAIN),
            new Game(FRANCE, IRELAND),
            new Game(ENGLAND, ICELAND) };
        Q = new Game[] {
            new Game(E[0], E[2]),
            new Game(E[1], E[5]),
            new Game(E[4], E[6]),
            new Game(E[3], E[7]) };
        S = new Game[] {
            new Game(Q[0], Q[1]),
            new Game(Q[2], Q[3]) };

        FINAL = new Game(S[0], S[1]);
    }

    private Game[] E, Q, S;
    private Game FINAL;

    static int poisson(double average) {
        double limit = Math.exp(-average);
        double product = 1d;

        int i;
        for (i = -1; product >= limit; i++) {
            product *= Math.random();
        }

        return i;
    }

    @Data
    static class Game {
        private final ObjectProperty<Team> teamHome;
        private int scoreHome;
        private final ObjectProperty<Team> teamGuest;
        private int scoreGuest;

        private final ObjectProperty<Team> winningTeam;

        public Game() {
            this((Team) null, null);
        }

        public Game(Team home, Team guest) {
            this.teamHome = new SimpleObjectProperty<>(home);
            this.teamGuest = new SimpleObjectProperty<>(guest);
            this.winningTeam = new SimpleObjectProperty<>(null);
        }

        public Game(Game homeWinner, Game guestWinner) {
            this((Team) null, null);
            this.teamHome.bind(homeWinner.winningTeamProperty());
            this.teamGuest.bind(guestWinner.winningTeamProperty());
        }

        public void play() {
            setScores(poisson(teamHome.get().getAvgScore().get() + teamGuest.get().getAvgReceived().get()),
                    poisson(teamGuest.get().getAvgScore().get() + teamHome.get().getAvgReceived().get()));
        }

        public void setScores(int scoreHome, int scoreGuest) {
            assert scoreGuest != scoreHome;

            this.scoreGuest = scoreGuest;
            this.scoreHome = scoreHome;

            this.teamGuest.get().playedGame(scoreGuest, scoreHome);
            this.teamHome.get().playedGame(scoreHome, scoreGuest);

            winningTeam.bind(scoreGuest > scoreHome ? teamGuest : teamHome);
        }

        public ObjectProperty<Team> winningTeamProperty() {
            return this.winningTeam;
        }
    }

    @Data
    static class Team {
        private final String name;
        private final IntegerProperty games;
        private final IntegerProperty scoreTotal;
        private final IntegerProperty receivedTotal;
        private final DoubleExpression avgScore;
        private final DoubleExpression avgReceived;

        public Team(String name, int scoreTotal, int receivedTotal, int games) {
            this.name = name;
            this.scoreTotal = new SimpleIntegerProperty(scoreTotal);
            this.receivedTotal = new SimpleIntegerProperty(receivedTotal);
            this.games = new SimpleIntegerProperty(games);
            this.avgScore = DoubleProperty.doubleExpression(Bindings.divide(this.scoreTotal, games));
            this.avgReceived = DoubleProperty.doubleExpression(Bindings.divide(this.receivedTotal, games));
        }

        public Team(String name, int scoreTotal, int receivedTotal) {
            this(name, scoreTotal, receivedTotal, 3);
        }

        public void playedGame(int score, int scoreOther) {
            this.scoreTotal.set(this.scoreTotal.get() + score);
            this.receivedTotal.set(this.receivedTotal.get() + scoreOther);
            this.games.set(this.games.get() + 1);
        }

        public String toString() {
            return "Team[" + name + "]";
        }
    }

    public static void main(String[] args) {
        new EuroChampionship();
    }
}
